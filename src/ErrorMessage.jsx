import React, { Component } from 'react';

export default class ErrorMessage extends React.Component {
    render() {
        return (<div className="invalid-feedback">
            {this.props.message}
        </div>)
    }
}