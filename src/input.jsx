import React, { Component } from 'react';
import ErrorMessage from './ErrorMessage';


export default class Input extends React.Component {
  render() {
    const {
      type,
      name,
      label,
      maxLength,
      required,
      valid,
      error,
    } = this.props
    const validClass = valid !== null ? (valid ? 'is-valid' : 'is-invalid') : ''

    return <div className="form-group">
      <label htmlFor={name}>{label}{required ? '*' : ''}</label>
      <input type={type} name={name} className={`form-control ${validClass}`} id={name} aria-describedby={`${name}Help`}
        placeholder={label} onChange={this.props.changeHandler} maxLength={maxLength} />
      {valid === false ? <ErrorMessage message={error} /> : null}
    </div>
  }
}

