import React, { Component } from 'react';
import './App.css';
import { file } from '@babel/types';
import Input from './input';



class App extends Component {
  firstTabList = ['firstname', 'lastname', 'email']
  state = {
    tab: 1,
    firstname: {
      type: 'text',
      name: 'firstname',
      label: 'Имя',
      value: '',
      maxLength: 255,
      required: true,
      regexp: null,
      valid: null,
      error: null
    },
    lastname: {
      type: 'text',
      name: 'lastname',
      label: 'Фамилия',
      value: '',
      maxLength: 255,
      required: true,
      regexp: null,
      valid: null,
      error: null
    },
    email: {
      type: 'email',
      name: 'email',
      label: 'Email',
      value: '',
      maxLength: 255,
      required: true,
      regexp: /[^@]+@[^\.]+\..+/g,
      valid: null,
      error: null
    },
    city: {
      type: 'text',
      name: 'city',
      label: 'Город',
      value: '',
      maxLength: 255,
      required: true,
      regexp: null,
      valid: null,
      error: null
    },
    index: {
      type: 'number',
      name: 'index',
      label: 'Постовый индекс',
      value: '',
      maxLength: 6,
      required: true,
      regexp: /^[0-9]*$/g,
      valid: null,
      error: null
    }
  }

  handleSubmit = e => {
    e.preventDefault()
    console.log('SUBMIT')
  }

  changeHandler = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    let field = this.state[name]
    console.log(field, name, value)
    field.value = value

    this.setState({ [name]: field })
  }

  clickNextHandler = e => {
    e.preventDefault()

    let isValid = true
    this.firstTabList.forEach((filedName) => {
      let result = this.validField(filedName)
      if (isValid && !result) {
        isValid = false
      }
    })

    if (isValid) {
      this.setState({ tab: 2 })
    }
    console.log('is valid ', isValid)
  }

  clickBack = e => {
    this.firstTabList.forEach((filedName) => {
      this.setState({
        [filedName]: {
          ...this.state[filedName],
          valid: null,
          error: '',
          value: ''
        }
      })
    })
    this.setState({ tab: 1 })

    console.log('clear form')
  }

  clickSubmit() {
    console.log('submit')
    let errorFunc = () => alert('Просьба повторить запрос позже')
    fetch('./test.php').then(function (response) {
      response.json().then(function (json) {
        if (json.success) {
          alert('Успешно отправлено')
        } else {
          errorFunc()
        }
      }).catch(errorFunc)
    }).catch(errorFunc)
  }

  validField(filedName) {
    let filed = this.state[filedName]
    let error = null
    let valid = true

    const { value, regexp, label, required } = filed

    if (valid && required && value.length === 0) {
      error = (<span>Поле <b>"{label}"</b> является обязательным для заполнения</span>)
      valid = false
    }

    if (valid && regexp instanceof RegExp && !regexp.test(value)) {
      error = `Неправельный формат`
      valid = false
    }

    filed.error = error
    filed.valid = valid
    this.setState({ [filedName]: filed })
    return valid
  }


  render() {

    return (
      <div className="App" >
        <form onSubmit={this.handleSubmit} >

          <ul className="nav nav-tabs" id="myTab" role="tablist">
            <li className="nav-item">
              <a className={`nav-link ${this.state.tab === 1 ? 'active' : ''}`} id="home-tab" href="#data" role="tab"
                aria-selected="true" onClick={this.clickBack}>Основные даные</a>
            </li>
            <li className="nav-item">
              <a className={`nav-link ${this.state.tab === 2 ? 'active' : ''}`} id="del-tab" href="#del" role="tab"
                aria-selected="false" onClick={this.clickNextHandler}>Адрес доставки</a>
            </li>
          </ul>

          <div className="tab-content" id="myTabContent">
            <div className={`tab-pane fade ${this.state.tab === 1 ? 'active show' : ''}`} id="data" role="tabpanel" >
              <Input {...this.state.firstname} changeHandler={this.changeHandler}></Input>
              <Input {...this.state.lastname} changeHandler={this.changeHandler}></Input>
              <Input {...this.state.email} changeHandler={this.changeHandler}></Input>
              <button type="button" className="btn btn-primary" onClick={this.clickNextHandler}>Продолжить</button>
            </div>

            <div className={`tab-pane fade ${this.state.tab === 2 ? 'active show' : ''}`} id="del" role="tabpanel">
              <Input {...this.state.city} changeHandler={this.changeHandler}></Input>
              <Input {...this.state.index} changeHandler={this.changeHandler}></Input>
              <button type="button" className="btn btn-primary" onClick={this.clickSubmit}>Отправить</button>
            </div>
          </div>
        </form>
      </div >
    );
  }

}

export default App;
